API Testing Scenario
API Test Framework Example in Java language using Java libraries;
RestAssured (for API Service) and Cucumber (for BDD styled),
along with Maven as a build tool.
Table of Contents
Overview
How to setup project
How to run tests and generate reports
Where to find Json Reports
Jenkins (CI/CD) Integration setup guideline

Overview
This API test framework uses Java as a main language with 'RestAssured' and 'Cucumber' libraries

Test will run according to feature files - which are stored under ./src/test/java/resources folder.
Outputs Json file will be produced under ./target/cucumber.json

How to setup
Pull (clone) source code from Git as below command:

https://gitlab.com/nareshkhunt/functionalweatherapi

Then tests can be run as mentioned in the next step.

How to run tests and generate reports

Once we run RunCukesTest
Once finished, there will be reports in ./target/cucumber.json file


Where to find reports
JSON file: ./target/cucumber.json


Jenkins (CI/CD) Integration setup guideline
In your new Jenkins Job:
Created Jenkin Job for maven style project
Configured git repository and credentials
Added maven build steps to run test cases
Post build generate the cucumber Report for analysis
Email report can be emailed if group mail id available